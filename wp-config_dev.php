<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'elmydemo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D>*M|(+n4bW?<$L=-n8-O!5b2p]{J_DfjE]kU2N}q~<Mr[LRd^dZz$yFad{K|4+f');
define('SECURE_AUTH_KEY',  'V0<S51>oNh!$h2++.#^&O`4+_QWPvioVrOF9JN=0pO;*NM? >sUZZ0Pby4NPf4$I');
define('LOGGED_IN_KEY',    'a0fouwatQ(F.K?U]/gL?vTf1K<g.vc;e}]D)8CvjCLs::r*a1Aw_*?1=n@BsB`41');
define('NONCE_KEY',        '%`dm[<6={%x61f CNzfS|FSWRJ)>)G*}rdUiG+`gpv<gijI12TgY@w@a9SS1)Ef9');
define('AUTH_SALT',        'Fj%iL@F cJdE}aIUyL3&<*/j@}3>Z5JQj]hg(wVHF>]eWF~B xA1!3>p.s!2r*-2');
define('SECURE_AUTH_SALT', 'G~A[T3mQD{Dt9-mNKOF;7-eV3u?BX-!nadg[&bR;6`<v{B-gR7m$=wJpZ1XI|pSZ');
define('LOGGED_IN_SALT',   '-~$Ji@l(QP8v{}]t?(?zeT,e3)`?bYm2v]{n8R>Zz/h6y0$S5[%z#ORpjELV6?~N');
define('NONCE_SALT',       'J{`E^E58p/><xYA+XB/Pe.^*A2V]l8dN&S[:~]RnZ[9<Z;^_~c*joAxpv[jGQZ1;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

