jQuery(document).ready(function(){
    $sticky = shm_vars.sticky;
    $message = shm_vars.message;
    $header = jQuery('header.site-header');    
    
    /**
     * Making header sticky 
     */
    if($sticky){
        $header.addClass('sticky');
        jQuery('.site-content').css('margin-top', $header.height());                
    }    
        
    /**
     * Adding the massage to header
     */
    if($message){
        $header.append('<div class="header-message">' + $message + '</div>')
    }
})