<?php
/**
 * Plugin Name: Sticky Header and Message
 * Plugin URI: https://bitbucket.org/Aibolit6677/elmydemo
 * Description: Add sticky header option and message to header
 * Version: 1.0
 * Author: Volkovich Boris
 * Text Domain:  sticky-header-message
 */

namespace StickyHeaderMessageNamespace;

if (!defined('ABSPATH')) {
    return;
}

/**
 * Main plugin class
 */
class StickyHeaderMessage {
    private $textdomain = 'sticky-header-message';
    private $prefix = 'sticky_header_message';
    protected static $_instance = null;
    
    /**
     * Ensuring there's only one instance
     */
    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function __construct() {
        $this->init_hooks();
    }
    
    /**
     * Registering hooks
     */
    public function init_hooks() {
        add_action('customize_register', [$this, 'register_settings']);
        add_action('wp_enqueue_scripts', [$this, 'register_script_style']);
    }

    /**
     * Adding options to theme settings
     */
    public function register_settings($wp_customize) {        
        $wp_customize->add_section($this->prefix . '_section' , array(
            'title'    => __('Sticky Header Message', $this->textdomain),
            'priority' => 30
        ) );   

        $wp_customize->add_setting($this->prefix . '_message_setting' , array(
            'default'   => '',
        ) );
        
        $wp_customize->add_setting($this->prefix . '_sticky_setting' , array(
            'default'   => false,
        ) );                       
        
        $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, $this->prefix .'_message_control', array(
            'label'    => __( 'Header Message', $this->textdomain ),
            'section'  => $this->prefix .'_section',
            'settings' => $this->prefix .'_message_setting',
        ) ) );

        $wp_customize->add_control( new \WP_Customize_Control( $wp_customize, $this->prefix .'_sticky_control', array(
            'label'    => __( 'Sticky Header', $this->textdomain ),
            'section'  => $this->prefix .'_section',
            'settings' => $this->prefix .'_sticky_setting',
            'type'     => 'checkbox'
        ) ) );
    }
    
    
    /**
     * Registering styles and scripts
     * and passing vars to JS
     */
    public function register_script_style(){
        wp_enqueue_style($prefix . '_style', plugins_url('/assets/css/sticky-header-message.css', __FILE__));
        wp_register_script($prefix . '_script', plugins_url('/assets/js/sticky-header-message.js', __FILE__), ['jquery'], 1.0, true);
        wp_localize_script($prefix . '_script', 'shm_vars', [
           'sticky'     =>  get_theme_mod($this->prefix . '_sticky_setting'),
           'message'    =>  get_theme_mod($this->prefix . '_message_setting')
        ]);        
        wp_enqueue_script($prefix . '_script');
    }        
}

StickyHeaderMessage::instance();

